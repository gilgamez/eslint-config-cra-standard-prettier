# Contributing

## Version bumping

After committing changes the maintainers of this package may manually bump the version using npm.

- Updating docs: patch
- Updating rule from 'error' to 'warn': minor
- Any other rule update: major
- Changes in (peer)dependencies: major (since we accept all minor versions already)

```shell
npm version major -m "Upgrade to %s because..."
```

> **Note:** this will create a commit with tag unless you run `npm --no-git-tag-version version ...`

## Publishing

After bumping the version, publish the package again: 

```
npm publish --access public
```

You may need to sign in to npm on the command line with `npm login`.
