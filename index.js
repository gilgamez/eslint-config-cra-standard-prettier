
module.exports = {
  extends: [
    'react-app',
    'standard',
    'standard-react',
    'plugin:prettier/recommended',
  ],
  plugins: [
    'header',
    'jest',
    'prettier',
  ],

  rules: {
    'camelcase': ['error', { 'allow': ['^UNSAFE_'] }],
    'no-console': ['warn', { allow: ['error', 'warn'] }],

    'prettier/prettier': ['error', {
      tabWidth: 2,
      useTabs: false,
      semi: false,
      singleQuote: true,
      trailingComma: 'all',
      arrowParens: 'always'
    }],
    'header/header': [
      2,
      'block',
      [
        '*',
        {
          'pattern': ' * Copyright © VNG Realisatie \\d{4}',
          'template': ' * Copyright © VNG Realisatie 2020'
        },
        ' * Licensed under the EUPL',
        ' '
      ]
    ],

    'react/no-unsafe': 'warn',
    'react/forbid-prop-types': ['error', { forbid: ['any'] }],
    'react/jsx-handler-names': 'warn', // TODO: change to ERROR after migration of all projects
    'react/jsx-pascal-case': ['error', { 'allowAllCaps': true }],
    'react/sort-comp': 'warn', // TODO: change to ERROR after migration of all projects

    'import/order': 'error',

    'jest/consistent-test-it': [
      'error',
      {
        'fn': 'test',
        'withinDescribe': 'it'
      }
    ],
    'jest/expect-expect': [
      'error',
      {
        'assertFunctionNames': [
          'expect'
        ]
      }
    ],
    'jest/no-jasmine-globals': 'error',
    'jest/no-test-callback': 'error',
    'jest/prefer-to-be-null': 'error',
    'jest/prefer-to-be-undefined': 'error',
    'jest/prefer-to-contain': 'error',
    'jest/prefer-to-have-length': 'error',
    'jest/valid-describe': 'error',
    'jest/valid-expect-in-promise': 'error'
  },
}
